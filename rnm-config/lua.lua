return {
	--- Maps
	basePath = "/home/server/my-super-server/legacy/", -- must end with a slash / or \
	mapHistory = 4, -- number of saved previous maps to do not play again
	mapLists = {
		-- will fallback to the first list if none can be used
		-- {"path", "name", minimumPlayers, maximumPlayers},
		-- {"smallMaps.txt", "Small maps:", 0, 10},
		-- {"bigMaps.txt", "Big maps:", 11, -1}, -- use a lower value like -1 for infinite
		{"maplist.txt", "Map list:", 0, -1},
	},
	fallbackMap = "oasis", -- when we fail to select a map if mapLists badly set

	--- Commands
	---- Client
	showNextCommandName = "?next",
	mapListCommandName = "?maps",
	mapHistoryCommandName = "?history",
	mapsPerLine = 6, -- maps per line in map list

	---- Console
	setNextMapCommandName = "setnextmap",
	randomNextMapCommandName = "randomnextmap",

	--- Chat
	chatCommand = "chat",
	chatMessage = "^dMode: ^3Random Map^d. Current map ^2%s^d. ^dNext map ^2%s^d.",
	chatInterval = 120, -- in seconds, 0 to disable

	--- Banner
	bannerCvar = "g_msg1", -- set to nil to disable
	bannerMessage = "^dMode: ^3Random Map^d. Current map ^2%s^d. ^dNext map ^2%s^d.",

	--- Cvar
	cvarPrefix = "rnm_", -- prefixed for each created cvar
}
