-- Random Next Map NG
-- SPDX-FileCopyrightText: 2018 Elouan Martinet <exa@elou.world>
-- SPDX-License-Identifier: BSD-3-Clause
--
-- Copyright (C) 2018 Elouan Martinet <exa@elou.world>
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 1. Redistributions of source code must retain the above copyright
-- notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived
-- from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
-- "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
-- LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
-- FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
-- COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
-- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
-- BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
-- LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
-- CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
-- STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
-- OF THE POSSIBILITY OF SUCH DAMAGE.

local version = "0.4.0"

-- Config

local config = require "rnm-config.lua"
assert(config)

-- Utils

--- get and check

function getCurrentMap()
	return et.trap_Cvar_Get("mapname")
end

function validMapName(mapname)
	return string.match(mapname, "^[0-9a-zA-Z-_]+$") ~= nil
end

function getNextMap()
	local nextmapCvar = et.trap_Cvar_Get("nextmap")
	if string.sub(nextmapCvar, 1, 4) ~= "map " then
		return nil
	end
	local nextmap = string.sub(nextmapCvar, 5)
	if not validMapName(nextmap) then
		return nil
	end
	return nextmap
end

function mapExists(mapname)
	if mapname == nil or mapname == false or not validMapName(mapname) then
		return false
	end
	local fd, len = et.trap_FS_FOpenFile("scripts/" .. mapname .. ".arena", et.FS_READ)
	et.trap_FS_FCloseFile(fd)
	if len ~= -1 then
		return true
	end
	return false
end

function getPlayerCount()
	local count = 0
	for i = 0, tonumber(et.trap_Cvar_Get("sv_maxClients")) - 1 do
		local cs = et.trap_GetConfigstring(et.CS_PLAYERS + i)
		local t = et.Info_ValueForKey(cs, "t")
		if t == "1" or t == "2" then
			count = count + 1
		end
	end
	return count
end

function inHistory(mapname)
	if not validMapName(mapname) then
		return false
	end
	local historyCvar = et.trap_Cvar_Get(config.cvarPrefix .. "history")
	if historyCvar == "" then
		return false
	end
	local lower = string.lower(mapname)
	for map in string.gmatch(historyCvar, "[^,]+") do
		if map == lower then
			return true
		end
	end
	return false
end

function isWarmup()
	local gamestate = et.trap_Cvar_Get("gamestate")
	if gamestate == "0" or gamestate == "3" then -- playing or intermission
		return false
	end
	return true
end

--- set and send

function setNextMap(mapname)
	if not mapExists(mapname) then
		return false
	end
	et.trap_Cvar_Set("nextmap", "map " .. string.lower(mapname))
	return true
end

function sendMessage(clientNum, message)
	if clientNum <= -1 then
		et.G_Print(message .. "\n")
	end
	if clientNum >= -1 then
		et.trap_SendServerCommand(clientNum, config.chatCommand .. " \"" .. message .. "\"")
	end
end

function setBanner(message)
	if config.bannerCvar then
		et.trap_Cvar_Set(config.bannerCvar, message)
	end
end

function addHistory(mapname)
	if not validMapName(mapname) then
		return
	end
	local lower = string.lower(mapname)
	local history = {}
	for map in string.gmatch(et.trap_Cvar_Get(config.cvarPrefix .. "history"), "[^,]+") do
		if #history < config.mapHistory and map ~= lower then
			table.insert(history, map)
		end
	end
	local historyCvar = lower
	for i = 1, #history do
		historyCvar = historyCvar .. "," .. history[i]
	end
	et.trap_Cvar_Set(config.cvarPrefix .. "history", historyCvar)
end

function coloredMapList(clientNum, maps)
	if #maps == 0 then
		return
	end
	local color = 1
	local line = ""
	local n = 0
	for id, mapname in ipairs(maps) do
		if line ~= "" then
			line = line .. " "
		end
		line = line .. "^" .. color .. mapname
		n = n + 1
		if n == config.mapsPerLine then
			sendMessage(clientNum, line)
			n = 0
			line = ""
		end
		if color < 9 then
			color = color + 1
			if color == 4 then
				color = 5
			end -- skip color 4 not goodly viewable
		else
			color = 1
		end
	end
	if line ~= "" then
		sendMessage(clientNum, line)
	end
end

-- Core

local chatIntervalLast = 0
local loadedMapLists
local frameLevelTime = 0

function sendNextMapMessage(clientNum)
	if isWarmup() then
		return
	end
	local nextmap = getNextMap()
	if nextmap == nil then
		nextmap = "<unknown>"
	end
	sendMessage(clientNum, string.format(config.chatMessage, getCurrentMap(), nextmap))
	if clientNum == -1 then
		chatIntervalLast = frameLevelTime
	end
end

function setNextMapBanner()
	if isWarmup() then
		return
	end
	setBanner(string.format(config.bannerMessage, getCurrentMap(), getNextMap()))
end

function sendMapListMessage(clientNum)
	for i = 1, #config.mapLists do
		local to = config.mapLists[i][4]
		if config.mapLists[i][4] < config.mapLists[i][3] then
			to = "infinite"
		end
		sendMessage(clientNum, config.mapLists[i][3] .. " to " .. to .. " players - " .. config.mapLists[i][2])
		if loadedMapLists[i] ~= nil then
			coloredMapList(clientNum, loadedMapLists[i])
		else
			sendMessage(clientNum, "^1Couldn't load map list!")
		end
	end
end

function sendMapHistoryMessage(clientNum)
	local historyCvar = et.trap_Cvar_Get(config.cvarPrefix .. "history")
	if historyCvar == "" then
		return
	end
	maps = {}
	for mapname in string.gmatch(et.trap_Cvar_Get(config.cvarPrefix .. "history"), "[^,]+") do
		table.insert(maps, mapname)
	end
	previousMaps = {}
	for i = 2, #maps do
		table.insert(previousMaps, maps[i])
	end
	coloredMapList(clientNum, previousMaps)
end

function setNextMapCommand(mapname)
	if isWarmup() then
		return true
	end
	if setNextMap(mapname) then
		setNextMapBanner()
		sendNextMapMessage(-1)
		return true
	end
	return false
end

function fallbackNextMap(reason)
	if not reason then
		reason = ""
	else
		reason = " (" .. reason .. ")"
	end
	sendMessage(-1, "^1Failed to select a random map" .. reason .. ", falling back to ^3" .. config.fallbackMap)
	setNextMapCommand(config.fallbackMap)
end

function loadMapList(mapListFile)
	local fd = io.open(config.basePath .. mapListFile)
	if fd == nil then
		return nil
	end
	local maps = {}
	for line in fd:lines() do
		local mapname = string.lower(line):gsub("^%s+", ""):gsub("%s+$", "")
		if mapExists(mapname) then
			table.insert(maps, mapname)
		end
	end
	table.sort(maps)
	return maps
end

function loadAllMapLists()
	loadedMapLists = {}
	for i = 1, #config.mapLists do
		loadedMapLists[i] = loadMapList(config.mapLists[i][1])
	end
end

function selectRandomNextMapFrom(id)
	if loadedMapLists[id] == nil then
		fallbackNextMap("cannot load file")
		return
	end
	local i = 0
	while 1 do
		nextmap = loadedMapLists[id][math.random(#loadedMapLists[id])]
		if not inHistory(nextmap) then
			if not setNextMapCommand(nextmap) then
				fallbackNextMap("internal not exists")
			end
			break
		end
		i = i + 1
		if i > 32 then
			fallbackNextMap("reached limit, map list too short?")
			break
		end
	end
end

function selectRandomNextMap()
	loadAllMapLists()
	if isWarmup() then
		return
	end
	local players = getPlayerCount()
	if #config.mapLists > 0 then
		for i = 1, #config.mapLists do
			if players >= config.mapLists[i][3] and (players <= config.mapLists[i][4] or config.mapLists[i][4] < config.mapLists[i][3]) then
				selectRandomNextMapFrom(i)
				return
			end
		end
		selectRandomNextMapFrom(1)
		return
	end
	fallbackNextMap("empty mapLists array")
end

function selectRandomNextMapFromList(userMapList)
	loadAllMapLists()
	if isWarmup() then
		return
	end
	needle = string.lower(userMapList)
	for i = 1, #config.mapLists do
		if string.find(string.lower(config.mapLists[i][2]), needle, 1, true) then
			sendMessage(-1, needle .. " -> " .. config.mapLists[i][2])
			selectRandomNextMapFrom(i)
			return
		end
	end
end

function et_InitGame(levelTime, randomSeed, restart)
	et.RegisterModname("RNM-NG " .. "v" .. version)
	math.randomseed(randomSeed)
	setBanner("")
	frameLevelTime = levelTime
	local currentMap = getCurrentMap()
	addHistory(currentMap)
	if restart == 0 or getNextMap() == nil then
		selectRandomNextMap()
	else
		loadAllMapLists()
		setNextMapBanner()
	end
end

function et_ClientCommand(clientNum, command)
	local offset = 0
	local name = string.lower(et.trap_Argv(offset))
	if name == "say" then
		offset = 1
		name = string.lower(et.trap_Argv(offset))
	end
	if name == config.showNextCommandName then
		sendNextMapMessage(clientNum)
	elseif name == config.mapListCommandName then
		sendMapListMessage(clientNum)
	elseif name == config.mapHistoryCommandName then
		sendMapHistoryMessage(clientNum)
	else
		return 0
	end
	return 1
end

function et_ConsoleCommand(command)
	if command == config.showNextCommandName then
		sendNextMapMessage(-2)
	elseif command == config.mapListCommandName then
		sendMapListMessage(-2)
	elseif command == config.mapHistoryCommandName then
		sendMapHistoryMessage(-2)
	elseif command == config.setNextMapCommandName then
		setNextMapCommand(et.trap_Argv(1))
	elseif command == config.randomNextMapCommandName then
		local arg = et.trap_Argv(1)
		if arg == "" then
			selectRandomNextMap()
		else
			selectRandomNextMapFromList(arg)
		end
	else
		return 0
	end
	return 1
end

function et_RunFrame(levelTime)
	frameLevelTime = levelTime
	if config.chatInterval > 0 and (chatIntervalLast + config.chatInterval * 1000) < levelTime then
		sendNextMapMessage(-1)
	end
end
