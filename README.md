# Random Next Map NG

RNM-NG is a Lua script for [Wolfenstein: Enemy Territory](https://www.splashdamage.com/games/wolfenstein-enemy-territory/). It can be used to randomly select a next map instead of relying on static map rotations or voting.

It should work with all major mods with Lua support. It has been tested successfully on legacy, [etbloat](https://etblo.at/) and Nitmod.

## Installation

To install RNM-NG, simply put `rnm.lua` and `rnm-config` in your mod directory.

Then, edit the file `rnm-config/lua.lua` with your settings.

Make sure to edit the value of `basePath` so that the directory contains your map lists.

Finally, edit your mod's cvars to add `rnm.lua` to the loaded scripts (e.g. `set lua_modules "rnm.lua"`).

A map list is a file containing a map (bsp name) per line.

### WolfAdmin support

To be able to use the `setnextmap` and `randomnextmap` commands from WolfAdmin, you can put the file from [wolfadmin/rnmng.lua](wolfadmin/rnmng.lua) at `luascripts/wolfadmin/commands/admin/rnmng.lua` in your mod directory. You can edit the `commands.addadmin` lines to configure the ACL required to use these commands (it is set to `nextmap`). Check the [WolfAdmin documentation](https://dev.timosmit.com/wolfadmin/extend.html) for more information.

Make sure that either:
* WolfAdmin admin commands are different from the RNM-NG commands (it is the case in the given file),
* RNM-NG script is loaded before WolfAdmin script.

If neither conditions are met, the server will get stuck when calling the commands.

## License

This script is released under BSD 3-Clause License, see [COPYING](COPYING) for the full license information
