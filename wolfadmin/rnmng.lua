local commands = require "luascripts.wolfadmin.commands.commands"

function setNextMap(clientId, command, ...)
	et.trap_SendConsoleCommand(et.EXEC_APPEND, "setnextmap " .. table.concat({...}, " "))
end

function randomNextMap(clientId, command, ...)
	et.trap_SendConsoleCommand(et.EXEC_APPEND, "randomnextmap " .. table.concat({...}, " "))
end

commands.addadmin("setnext", setNextMap, "nextmap", "Set the next map")
commands.addadmin("random", randomNextMap, "nextmap", "Randomly select the next map")
